﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GIT_Opdracht
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void goudsbloem_MouseEnter(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.Orange);
		}

		private void rozen_MouseEnter(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.Red);
		}

		private void wilkensbitter_MouseEnter(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.Yellow);
		}

		private void lavandel_MouseEnter(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.Purple);
		}

		private void rozen_MouseLeave(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.LightGray);
		}

		private void goudsbloem_MouseLeave(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.LightGray);
		}

		private void wilkensbitter_MouseLeave(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.LightGray);
		}

		private void lavendel_MouseLeave(object sender, MouseEventArgs e)
		{
			this.Background = new SolidColorBrush(Colors.LightGray);
		}
	}
}
